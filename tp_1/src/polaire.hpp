#ifndef _POLAIRE_
#define _POLAIRE_
#include "point.hpp"
#include "cartesien.hpp"
#include <sstream>
#include <cmath>
class Polaire : public Point {
    private:
    double angle, distance;
    public:
    Polaire(double angle=0.0, double distance=0.0);
    Polaire(Cartesien& c);
    double getAngle() const;
    void setAngle(double angle);
    double getDistance() const;
    void setDistance(double distance);
    void afficher(std::stringstream& ss) const;
    void convertir(Cartesien& c) const;
    void convertir(Polaire& p) const ;
};
#endif