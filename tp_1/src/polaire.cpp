#include "polaire.hpp"

Polaire::Polaire(double a, double d): angle(a), distance(d){}

Polaire::Polaire(Cartesien& c){
    c.convertir(*this);
}

double Polaire::getAngle() const {
    return angle;
}

double Polaire::getDistance() const {
    return distance;
}

void Polaire::setAngle(double a){
    angle = a;
}

void Polaire::setDistance(double d){
    distance = d;
}

void Polaire::afficher(std::stringstream& ss) const{
    ss << "(a=" << angle << ";d=" << distance << ")";
}

void Polaire::convertir(Cartesien& c) const {
    c.setX(distance*std::cos(angle*(M_PI/180)));
    c.setY(distance*std::sin(angle*(M_PI/180)));
}

void Polaire::convertir(Polaire& p) const  {
    p.angle = angle;
    p.distance = distance;
}