#include "cartesien.hpp"

Cartesien::Cartesien(double a, double b): x(a), y(b){}

Cartesien::Cartesien(Polaire& p){
    p.convertir(*this);
}

double Cartesien::getX() const {
    return x;
}

double Cartesien::getY() const {
    return y;
}

void Cartesien::setX(double a){
    x = a;
}

void Cartesien::setY(double b){
    y = b;
}

void Cartesien::afficher(std::stringstream& ss) const{
    ss << "(x=" << x << ";y=" << y << ")";
}

void Cartesien::convertir(Cartesien& c) const {
    c.x = x;
    c.y = y;
}

void Cartesien::convertir(Polaire& p) const {
    p.setAngle(std::atan(y/x)*(180.0/3.141592653589793238463));
    p.setDistance(std::sqrt(std::pow(x,2)+std::pow(y,2)));
}
