#include "point.hpp"

std::stringstream& operator<< (std::stringstream& ss, const Point& p ) { 
    p.afficher(ss);
    return ss;
}