#ifndef _POINT_
#define _POINT_
#include <sstream> 
class Cartesien;
class Polaire;
class Point{
    public :
    virtual void afficher(std::stringstream& ss) const = 0;
    virtual void convertir(Cartesien& c) const = 0;
    virtual void convertir(Polaire& p) const = 0;
};
std::stringstream& operator<< (std::stringstream& ss,const  Point& p) ;
#endif