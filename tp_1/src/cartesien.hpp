#ifndef _CARTESIEN_
#define _CARTESIEN_
#include "point.hpp"
#include "polaire.hpp"
#include <sstream>
class Cartesien : public Point {
    private:
    double x, y;
    public:
    Cartesien(double x=0.0, double y=0.0);
    Cartesien(Polaire& p);
    double getX() const;
    void setX(double x);
    double getY() const;
    void setY(double y);
    void afficher(std::stringstream& ss) const;
    void convertir(Cartesien& c) const;
    void convertir(Polaire& p) const;
};
#endif